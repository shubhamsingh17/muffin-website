/**
 * Created by chitra on 3/8/17.
 */

(function() {
    'use strict';
    angular
        .module('app')
        .controller('ProfileController', ProfileController);

    ProfileController.$inject = ['$scope', '$state', '$timeout','$http', 'network','$localStorage'];

    function ProfileController($scope, $state, $timeout, $http, network, $localStorage) {

        $scope.requestChange = function () {
            $scope.showProgress = true;
            network.editProfileRequestApiCall($localStorage.userId)
                .then(function(res) {
                    // console.log(res.data);
                    if (res.data && !res.data.isError) {

                        swal({
                            title: 'Success!',
                            text: 'Your change has been requested',
                            type: 'success',
                            confirmButtonText: 'OK'
                        });

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    console.log(err);
                    $scope.showProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                        $scope.logoutUser();

                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        $scope.profileDetails = {};
        $scope.getUserProfile = function () {
            $scope.showProgress = true;
            network.getProfileApiCall($localStorage.userId)
                .then(function(res) {
                    // console.log(res.data);
                    if (res.data && !res.data.isError) {

                        $scope.profileDetails = res.data.data[0];

                        $scope.profileImg = "https://s3-ap-southeast-1.amazonaws.com/muffdata/" + $scope.profileDetails._id;
                        network.getCreditReport({
                            'user_id':$scope.profileDetails._id,
                            'user_type':'muffin'
                        })
                        .then(function(res){
                            if(res.data.url){
                                $scope.profileDetails.url=res.data.url;
                            }
                            
                            if (res.data.score) {
                                $scope.profileDetails.credit_score=res.data.score;    
                            }
                            

                        },function(err){
                            console.log(err);
                        })
                        
                    }else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    console.log(err);
                    $scope.showProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                        $scope.logoutUser();

                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };
        $scope.getUserProfile();

        $scope.backButtonCLicked = function () {
            $scope.$parent.goToMuffins();
        };

        $scope.consent=function(){
            swal({
                html: '<img src="assets/images/Experian_logo.svg" style="width:150px;" ><div>To get credit score,you will have to enter personal identification details.Do you agree?</div>',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes',
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            })
            .then(function(){

            
                window.location=$scope.profileDetails.url;
            }).catch(function(reason){
                
            });
        }
    }

})();