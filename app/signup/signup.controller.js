(function() {
	'use strict';
	angular
	.module('app')
	.controller('SignupController', SignupController);

	SignupController.$inject = ['$scope', '$state', '$timeout','$http','network','$localStorage','$stateParams'];

	function SignupController($scope, $state, $timeout, $http, network, $localStorage, $stateParams) {

		$scope.goToLogin = function () {
			$scope.$parent.goToLogin();
		};

		//signup step 1
		$scope.signUpStep1 = true;
		$scope.signUpStep2 = false;
		$scope.signUpStep3 = false;

		$scope.signUp = {};
		$scope.signUp.name ="";
		$scope.signUp.email = "";
		$scope.signUp.username = "";
		$scope.signUp.password = "";
		$scope.signUp.phone = "";

		if($stateParams.name.length > 0){
			$scope.signUp.name = $stateParams.name;
			$scope.signUp.email = $stateParams.email;
			// console.log($stateParams.profileImg);
			$scope.profileImg = $stateParams.profileImg;
		}

		$scope.showProgress = false;

		$scope.nextToOtpScreen = function () {
			if($scope.signUp.name && $scope.signUp.name.length > 0){
				if($scope.signUp.email && $scope.signUp.email.length > 0){
					if($scope.isEmailValid($scope.signUp.email)){
						if($scope.signUp.username && $scope.signUp.username.length > 0){
							if($scope.signUp.password && $scope.signUp.password.length > 0){
								if($scope.signUp.phone && $scope.signUp.phone.length > 0){

									$scope.signUpApiCall();
								}else {
									swal({
										title: 'Error!',
										text: "Please enter Mobile",
										type: 'error',
										confirmButtonText: 'OK'
									});
								}
							}else {
								swal({
									title: 'Error!',
									text: "Please enter Password",
									type: 'error',
									confirmButtonText: 'OK'
								});
							}
						}else {
							swal({
								title: 'Error!',
								text: "Please enter UserName",
								type: 'error',
								confirmButtonText: 'OK'
							});
						}
					}else {
						swal({
							title: 'Error!',
							text: "Please enter valid Email",
							type: 'error',
							confirmButtonText: 'OK'
						});
					}
				}else {
					swal({
						title: 'Error!',
						text: "Please enter Email",
						type: 'error',
						confirmButtonText: 'OK'
					});
				}
			}else {
				swal({
					title: 'Error!',
					text: "Please enter Name",
					type: 'error',
					confirmButtonText: 'OK'
				});
			}
		};

		//signup step 2
		$scope.otp = {};
		$scope.otp.firstChar = "";
		$scope.otp.secondChar = "";
		$scope.otp.thirdChar = "";
		$scope.otp.fourthChar = "";
		$scope.otpFilled = false;
		$scope.otpSend = true;

		$scope.backButtonCLicked = function () {
			$scope.signUpStep1 = true;
			$scope.signUpStep2 = false;

		};

		$scope.submitMobile = function () {
			if($scope.signUp.phone && $scope.signUp.phone.length > 0 ){
				// console.log("otp send");
				$scope.otpSend = true;
			}else {
				swal({
					title: 'Error!',
					text: "Please enter Mobile Number",
					type: 'error',
					confirmButtonText: 'OK'
				});
			}
		};

		$scope.editMobileNumber = function () {
			$scope.otpSend = false;
		};

		$scope.verifyMobile = function () {
			if($scope.signUp.phone && $scope.signUp.phone.length > 0 ){
				if($scope.otp.firstChar.length != 0 && $scope.otp.secondChar.length != 0 && $scope.otp.thirdChar.length !=0 && $scope.otp.fourthChar.length != 0){
					if(angular.equals(($scope.otp.firstChar + $scope.otp.secondChar +$scope.otp.thirdChar+$scope.otp.fourthChar), $scope.newOtp)){

						$scope.verifyOtp();
					}else {
						swal({
							title: 'Error!',
							text: "Please enter valid otp",
							type: 'error',
							confirmButtonText: 'OK'
						});
					}
				}else {
					swal({
						title: 'Error!',
						text: "Please enter valid otp",
						type: 'error',
						confirmButtonText: 'OK'
					});
				}
			}else {
				swal({
					title: 'Error!',
					text: "Please enter Mobile Number",
					type: 'error',
					confirmButtonText: 'OK'
				});
			}
		};

		$scope.$watch('otp.fourthChar', function(value) {
			if($scope.otp.firstChar.length != 0 && $scope.otp.secondChar.length != 0 && $scope.otp.thirdChar.length !=0 && $scope.otp.fourthChar.length != 0){
				$scope.otpFilled = true;
			}
		}, true);


		// signUp ApiCall
		$scope.signUpApiCall = function () {
			// console.log($scope.signUp);
			$scope.showProgress = true;
			network.signupApiCall($scope.signUp)
			.then(function(res) {
				// console.log(res.data);

				if (res.data && !res.data.isError) {
					if(res.data.message){

						$localStorage.authToken = res.data.data.token;
						$localStorage.userId = res.data.data.user._id;
						$localStorage.email = res.data.data.user.email;
						$localStorage.name = res.data.data.user.name;

						//step 2 dialog appear
						$scope.signUpStep1 = false;
						$scope.signUpStep2 = true;
						$scope.signUpStep3 = false;

						$scope.sendOtp();
					}
				} else if (res.data.isError) {
					swal("Oops...", res.data.error, "error");
				}else{
					swal("Oops...", "Unable to parse server response", "error");
				}
				$scope.showProgress = false;
			}, function(err) {
				// console.log("signupApiCall", err);
				$scope.showProgress = false;
				if(err.status === 401){
					swal("Oops...", err.data.error, "error");
				}else {
					swal("Oops...", "Something went wrong. Please try again.", "error");
				}
			});
		};
		

		// sendOtp ApiCall
		$scope.sendOtp = function () {

			var a = Math.floor(100000 + Math.random() * 900000);
			$scope.newOtp = a.toString().substring(0, 4);

			$scope.showProgress = true;
			network.sendOtpApiCall($scope.newOtp, $scope.signUp.phone)
				.then(function(res) {
					// console.log(res.data);

					$scope.showProgress = false;
				}, function(err) {
					// console.log("sendOtpApiCall", err);
					$scope.showProgress = false;
					// if(err.status === 401){
					// 	swal("Oops...", err.data.error, "error");
					// }else {
					// 	swal("Oops...", "Something went wrong. Please try again.", "error");
					// }
				});
		};

		$scope.generateOtp = function () {
			
		};

		// verify ApiCall
		$scope.verifyOtp = function () {
			// console.log($scope.signUp);

			$scope.verifyOtpData ={};
			$scope.verifyOtpData.otp = $scope.otp.firstChar + $scope.otp.secondChar +$scope.otp.thirdChar+$scope.otp.fourthChar;
			$scope.verifyOtpData.phone = $scope.signUp.phone;
			$scope.verifyOtpData.user_id = $localStorage.userId ;

			// console.log($scope.verifyOtpData);
			$scope.showProgress = true;
			network.verifyOtpApiCall($scope.verifyOtpData)
				.then(function(res) {
					// console.log(res.data);

					if (res.data && !res.data.isError) {
						if(res.data.data.message){

							//step 3 dialog appear
							$scope.signUpStep1 = false;
							$scope.signUpStep2 = false;
							$scope.signUpStep3 = true;

						}

					} else if (res.data.isError) {
						swal("Oops...", res.data.error, "error");
					}else{
						swal("Oops...", "Unable to parse server response", "error");
					}
					$scope.showProgress = false;
				}, function(err) {
					// console.log("verifyOtpApiCall", err);
					$scope.showProgress = false;
					if(err.status === 401){
						swal("Oops...", err.data.error, "error");
					}else {
						swal("Oops...", "Something went wrong. Please try again.", "error");
					}
				});
		};

		// save Pin ApiCall
		$scope.savePin = function () {
			if($scope.signUp.username && $scope.signUp.username.length > 0 ){
				if($scope.signUp.pin && $scope.signUp.pin.length > 0 ){

					$scope.data = {};
					$scope.data.username = $scope.signUp.username;
					$scope.data.pin = $scope.signUp.pin;

					// console.log("setPin:", $scope.data);
					$scope.showProgress = true;
					network.setPinApiCall($scope.data)
						.then(function(res) {
							// console.log(res.data);

							if (res.data && !res.data.isError) {
								if(res.data.message){
									swal({
										title: 'Success!',
										text: res.data.message,
										type: 'success',
										confirmButtonText: 'OK'
									});
								}

								$scope.goToMuffins();

							} else if (res.data && res.data.isError) {
								swal("Oops...", res.data.error, "error");
							}else{
								swal("Oops...", "Unable to parse server response", "error");
							}
							$scope.showProgress = false;
						}, function(err) {
							// console.log("setPinApiCall", err);
							$scope.showProgress = false;
							if(err.status === 401){
								swal("Oops...", err.data.error, "error");
							}else {
								swal("Oops...", "Something went wrong. Please try again.", "error");
							}
						});

				}else {
					swal({
						title: 'Error!',
						text: "Please enter 4-digit Pin",
						type: 'error',
						confirmButtonText: 'OK'
					});
				}
			}else {
				swal({
					title: 'Error!',
					text: "Please enter Username",
					type: 'error',
					confirmButtonText: 'OK'
				});
			}
		};

		$("#phoneNumber").keydown(function (e) {
			// Allow: backspace, delete, tab, escape, enter and .
			if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
				// Allow: Ctrl/cmd+A
				(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
				// Allow: Ctrl/cmd+C
				(e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
				// Allow: Ctrl/cmd+X
				(e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
				// Allow: home, end, left, right
				(e.keyCode >= 35 && e.keyCode <= 39)) {
				// let it happen, don't do anything
				return;
			}
			// Ensure that it is a number and stop the keypress
			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
		});
		
		$("#pin").keydown(function (e) {
			// Allow: backspace, delete, tab, escape, enter and .
			if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
				// Allow: Ctrl/cmd+A
				(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
				// Allow: Ctrl/cmd+C
				(e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
				// Allow: Ctrl/cmd+X
				(e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
				// Allow: home, end, left, right
				(e.keyCode >= 35 && e.keyCode <= 39)) {
				// let it happen, don't do anything
				return;
			}
			// Ensure that it is a number and stop the keypress
			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
		});

		$("#alphaonly").on("keydown", function(event){

			// console.log("alpha");
			// Allow controls such as backspace, tab etc.
			var arr = [8,9,16,17,20,32,35,36,37,38,39,40,45,46];

			// Allow letters
			for(var i = 65; i <= 90; i++){
				arr.push(i);
			}

			// Prevent default if not in array
			if(jQuery.inArray(event.which, arr) === -1){
				event.preventDefault();
			}
		});

		$scope.isEmailValid = function(email) {
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		};

		$(".inputs").keyup(function () {
			// console.log("value", this.value.length);
			if (this.value.length == this.maxLength) {
				$(this).next('.inputs').focus();
			}
		});
		
	}

})();