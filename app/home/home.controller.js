(function() {
    'use strict';
    angular
        .module('app')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', '$state', '$timeout', '$http', '$localStorage', 'network', '$mdpDatePicker', '$mdpTimePicker', 'socket', '$mdToast', 'ngToast'];

    function HomeController($scope, $state, $timeout, $http, $localStorage, network, $mdpDatePicker, $mdpTimePicker, socket, $mdToast, ngToast) {

        $scope.userName = $localStorage.name;
        $scope.phone=$localStorage.phone;
        

        $scope.profileSelected = function(type) {
            // console.log(type);
            if (angular.equals(type, 'Profile')) {

                $scope.$parent.goToProfile();
            } else if (angular.equals(type, 'Logout')) {
                $scope.logoutUser();
            }
        };

        var imgUrl = "https://s3-ap-southeast-1.amazonaws.com/muffdata/";

        var windowHeight;
        $(document).ready(function() {
            function setHeight() {
                windowHeight = $(window).innerHeight();
                $('.muffins-body').css('height', windowHeight);
            }
            setHeight();

            $(window).resize(function() {
                setHeight();
            });
        });

        //Select Muffin
        $scope.selectedMuffinType = "";
        $scope.selectedMuffinIndex = "";
        $scope.selectedMuffinId = "";

        $scope.muffinClicked = function(selected, muffinIndex, muffinId) {
            $scope.selectedMuffinType = selected;
            $scope.selectedMuffinIndex = muffinIndex;
            $scope.selectedMuffinId = muffinId;

            if (angular.equals($scope.selectedMuffinType, 'bidding_muffins')) {

                $scope.connectSocket();
                $scope.getMuffinDetails();
                $scope.getCurrentAuctions(muffinId);

            } else if (angular.equals($scope.selectedMuffinType, 'active_muffins')) {

                $scope.getMuffinsMembers();
                $scope.getMuffinDetails();
                $scope.downloadAgreement()

            } else {
                $scope.getMuffinsMembers();
                $scope.getMuffinDetails();
            }

        };

        // GET ALL MUFFINS Api Call
        $scope.getMuffins = function() {
            $scope.muffins = [];
            $scope.biddingMuffins = [];
            $scope.invitedMuffins = [];
            $scope.activeMuffins = [];
            $scope.inProgressMuffins = [];

            $scope.showProgress = true;
            network.getMuffins($scope.data)
                .then(function(res) {
                    // console.log("all muffins",res.data);
                    if (res.data && !res.data.isError) {

                        $scope.muffins = res.data.data;
                        for (var i = 0; i < $scope.muffins.length; i++) {

                            if ($scope.muffins[i].type === 'in_progress_muffins') {
                                $scope.inProgressMuffins = $scope.muffins[i].muffin_details;

                            } else if ($scope.muffins[i].type === 'active_muffins') {
                                $scope.activeMuffins = $scope.muffins[i].muffin_details;

                            } else if ($scope.muffins[i].type === 'bidding_muffins') {
                                $scope.biddingMuffins = $scope.muffins[i].muffin_details;

                            } else if ($scope.muffins[i].type === 'invited_muffins') {
                                $scope.invitedMuffins = $scope.muffins[i].muffin_details;

                            }
                        }

                        if ($scope.biddingMuffins.length > 0) {
                            $scope.muffinClicked('bidding_muffins', 0, $scope.biddingMuffins[0].muffin_detail._id);

                        } else if ($scope.invitedMuffins.length > 0) {
                            $scope.muffinClicked('invited_muffins', 0, $scope.invitedMuffins[0].muffin_detail._id);

                        } else if ($scope.activeMuffins.length > 0) {
                            $scope.muffinClicked('active_muffins', 0, $scope.activeMuffins[0].muffin_detail._id);

                        } else if ($scope.inProgressMuffins.length > 0) {
                            $scope.muffinClicked('in_progress_muffins', 0, $scope.inProgressMuffins[0].muffin_detail._id);
                        }


                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    } else {
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if (err.status === 401) {
                        swal("Oops...", err.data.error, "error");
                        $scope.logoutUser();
                    } else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        $scope.getActivitiesMuffins = function() {
            $scope.activitiesMuffin = [];

            network.getMuffins($scope.data)
                .then(function(res) {
                    // console.log("all muffins",res.data);
                    if (res.data && !res.data.isError) {

                        for (var i = 0; i < res.data.data.length; i++) {
                            if (res.data.data[i].type === 'in_progress_muffins') {
                                $scope.activitiesMuffin = res.data.data[i];
                            }
                        }

                        // console.log($scope.activitiesMuffin.length);

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    } else {
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if (err.status === 401) {
                        swal("Oops...", err.data.error, "error");
                        $scope.logoutUser();

                    } else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        // Get Muffin Details Api Call
        $scope.getMuffinDetails = function() {
            $scope.muffinDetails = {};
            $scope.showInprocessProgress = true;
            network.getInprocessMuffinsDetailsApiCall($scope.selectedMuffinId)
                .then(function(res) {
                    // console.log("MuffinDetails",res.data);
                    if (res.data && !res.data.isError) {

                        if (angular.equals($scope.selectedMuffinType, 'bidding_muffins')) {
                            $scope.biddingMuffinBids = [];
                            $scope.biddingMuffinDetails = res.data.data.auctions[0];
                            $scope.biddingMuffinBids = res.data.data.auctions[0].bids;
                            // console.log($scope.biddingMuffinBids);
                        } else {
                            $scope.muffinDetails = res.data.data;
                        }

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    } else {
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showInprocessProgress = false;
                }, function(err) {
                    $scope.showInprocessProgress = false;
                    if (err.status === 401) {
                        swal("Oops...", err.data.error, "error");
                        $scope.logoutUser();

                    } else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        // Get Muffin Member Details Api Call
        $scope.getMuffinsMembers = function() {
            $scope.muffinMembers = [];
            $scope.showMembersProgress = true;
            network.getMuffinsMembersApiCall($scope.selectedMuffinId)
                .then(function(res) {
                    // console.log("MuffinsMembers",res.data);
                    if (res.data && !res.data.isError) {

                        $scope.muffinMembers = res.data.data;
                        if (angular.equals($scope.selectedMuffinType, 'in_progress_muffins')) {
                            for (var i = 0; i < $scope.muffinMembers.length; i++) {
                                $scope.muffinMembers[i].img = imgUrl + $scope.muffinMembers[i].userId._id;
                                // console.log($scope.muffinMembers[i].img);

                                if ($scope.muffinMembers[i].userId.isDefaulter) {
                                    $scope.muffinMembers[i].type = "Defaulter";
                                } else if ($scope.muffinMembers[i].userId.kycDone && $scope.muffinMembers[i].userId.creditAssessmentDone) {
                                    $scope.muffinMembers[i].type = "Joined";
                                } else {
                                    $scope.muffinMembers[i].type = "Processing Application";
                                }
                            }
                        } else if (angular.equals($scope.selectedMuffinType, 'active_muffins')) {

                        }

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    } else {
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showMembersProgress = false;
                }, function(err) {
                    $scope.showMembersProgress = false;
                    if (err.status === 401) {
                        swal("Oops...", err.data.error, "error");
                        $scope.logoutUser();

                    } else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        //schedule meeting
        $scope.currentDate = new Date();
        var minDate = new Date();
        $scope.scheduleClicked = function(ev, muffinId) {

            var dateAndTime = "";
            $mdpDatePicker($scope.currentDate, {
                minDate: minDate,
                targetEvent: ev
            }).then(function(selectedDate) {

                dateAndTime = moment(selectedDate).format("DD/MM/YYYY") + ",";
                // console.log(dateAndTime);

                $mdpTimePicker(
                    moment(new Date()).format('HH:mm'), {
                        targetEvent: ev
                    }).then(function(selectedDate) {

                    dateAndTime = dateAndTime + " " + moment(selectedDate).format("HH:mm");
                    // console.log(dateAndTime);
                    $scope.scheduleMeeting(muffinId, dateAndTime);

                });
            });
        };

        // Schedule meeting Api Call
        $scope.scheduleMeeting = function(muffinId, dateAndTime) {
            $scope.data = {};
            $scope.data.muffinId = muffinId;
            $scope.data.userId = $localStorage.userId;
            $scope.data.rescheduledDateTime = dateAndTime;
            // console.log("schedule", $scope.data);

            $scope.showScheduleProgress = true;
            network.scheduleMeetingApiCall($scope.data)
                .then(function(res) {
                    // console.log("scheduleMeeting", res.data);
                    if (res.data && !res.data.isError) {

                        swal({
                            title: 'Success!',
                            text: "Successfully done!!!",
                            type: 'success',
                            confirmButtonText: 'OK'
                        });

                        $scope.getMuffinDetails();

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    } else {
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showScheduleProgress = false;
                }, function(err) {
                    $scope.showScheduleProgress = false;
                    if (err.status === 401) {
                        swal("Oops...", err.data.error, "error");
                        $scope.logoutUser();

                    } else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        $scope.biddingMuffinSubmitBtn = false;

        // Bidding Muffin
        $scope.biddingMuffinTickClicked = function() {
            if ($scope.bidding.value.length > 0) {
                $scope.biddingMuffinSubmitBtn = true;
            } else {
                swal("Oops...", "Please Enter a valid bid", "error");
            }
        };

        $scope.isValidBid = function(chitVal) {

            var highestBid = parseInt($scope.highestBid);
            var chitValue = chitVal;
            var bidValue = parseInt($scope.bidding.value);
            $scope.bidValue = bidValue;
            // console.log(bidValue);

            if (bidValue <= 0) {
                swal("Oops...", "Please Enter a valid bid", "error");
                return false;

            } else if (bidValue > chitValue) {
                swal("Oops...", "Enter a bid below the face value", "error");
                return false;

            } else if (highestBid == 0.7 * chitValue) {
                swal("Oops...", "Bidding Ended, minimum bid reached, leading bidder wins", "error");
                return false;

            } else if (bidValue > highestBid) {
                swal("Oops...", "Please Enter a bid less than the leading bid", "error");
                return false;

            } else if (bidValue < 0.7 * chitValue) {
                swal("Oops...", "Minimum bid is " + 0.7 * chitValue + "Please enter the bid above the minimum bid", "error");
                return false;

            } else if (bidValue % 100 != 0) {
                swal("Oops...", "Please Enter a bid in multiples of 100", "error");
                return false;

            } else if (highestBid == 0) {
                return true;

            } else if (bidValue < highestBid) {
                return true;

            } else {
                return false;

            }
        };

        $scope.$watch("bidding.value", function(newVal, oldVal) {

            if (newVal.length > 0) {
                var isnum = /^\d+$/.test(newVal);
                if (!isnum) {
                    swal("Oops...", "Enter a bid only numbers", "error");
                }
            }

        });

        $scope.submitBid = function(chitVal) {

            if ($scope.isValidBid(chitVal)) {
                $scope.data = {};
                $scope.data.userId = $localStorage.userId;
                $scope.data.auctionId = $scope.auctionId;
                $scope.data.bid = $scope.bidValue;

                // console.log($scope.data);

                socket.emit("new message", $scope.data);

                $scope.getCurrentAuctions($scope.data.auctions);

                $scope.bidding = {};
                $scope.bidding.value = "";
            }
        };

        //Invite Friends
        $scope.inviteFriends = function(regNo) {
            $scope.regNumber = regNo;
            $('.inviteFriendsModal').appendTo("body").modal('show');
        };

        $scope.inviteFriendsCancel = function() {
            $('.inviteFriendsModal').appendTo("body").modal('hide');
        };

        //Accept Decline Invited Muffins Api Call
        $scope.acceptDeclineInvitedMuffin = function(type, muffinId) {

            $scope.data = {};
            $scope.data.userId = $localStorage.userId;
            $scope.data.muffinId = muffinId;
            $scope.data.relationType = type;

            // console.log($scope.data);
            $scope.showProgress = true;
            network.acceptDeclineApiCall($scope.data)
                .then(function(res) {
                    // console.log("acceptDeclineApiCall", res.data);
                    if (res.data && !res.data.isError) {

                        swal({
                            title: 'Success!',
                            text: res.data.message,
                            type: 'success',
                            confirmButtonText: 'OK'
                        });

                        $('.muffinModal').appendTo("body").modal('hide');

                        $scope.getMuffins();

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    } else {
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if (err.status === 401) {
                        swal("Oops...", err.data.error, "error");
                        $scope.logoutUser();

                    } else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        //Join Available Muffins Api Call
        $scope.joinAvailableMuffin = function() {
            $scope.availableMuffins = [];

            $scope.showJoinMuffinProgress = true;
            network.getAvailableMuffinsApiCall($localStorage.userId)
                .then(function(res) {
                    // console.log("getAvailableMuffinsApiCall", res.data);
                    if (res.data && !res.data.isError) {

                        $scope.availableMuffins = res.data.data.muffins;

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    } else {
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showJoinMuffinProgress = false;
                }, function(err) {
                    $scope.showJoinMuffinProgress = false;
                    if (err.status === 401) {
                        swal("Oops...", err.data.error, "error");
                        $scope.logoutUser();

                    } else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        // Enter invite code
        $scope.invitedMuffinCode = {};
        $scope.invitedMuffinCode.name = "";

        // add Invited Muffin Api Call
        $scope.addInvitedMuffin = function() {
            // console.log($scope.invitedMuffinCode.name);
            if ($scope.invitedMuffinCode.name && $scope.invitedMuffinCode.name.length > 0) {
                $scope.data = {};
                $scope.data.userId = $localStorage.userId;
                $scope.data.relationType = "pending";
                $scope.data.muffinId = $scope.invitedMuffinCode.name;
                // console.log($scope.data);

                $scope.showProgress = true;
                network.addInvitedMuffinApiCall($scope.data)
                    .then(function(res) {
                        // console.log("getAvailableMuffinsApiCall", res.data);
                        if (res.data && !res.data.isError) {


                            swal({
                                title: 'Success!',
                                text: res.data.message,
                                type: 'success',
                                confirmButtonText: 'OK'
                            });
                            $('.muffinModal').appendTo("body").modal('hide');

                            $scope.getMuffins();

                        } else if (res.data.isError) {
                            swal("Oops...", res.data.error, "error");
                        } else {
                            swal("Oops...", "Unable to parse server response", "error");
                        }
                        $scope.showProgress = false;
                    }, function(err) {
                        $scope.showProgress = false;
                        console.log(err);
                        if (err.status === 401) {
                            swal("Oops...", err.data.error, "error");
                            $scope.logoutUser();

                        } else {
                            swal("Oops...", "Something went wrong. Please try again.", "error");
                        }
                    });
            } else {
                swal({
                    title: 'Error',
                    text: "Enter muffin code",
                    type: 'error',
                    confirmButtonText: 'OK'
                });
            }

        };

        // ADD NEW MUFFIN APICALL
        $scope.addMuffin = {};
        $scope.addMuffin.name = "";
        $scope.addMuffin.chitValue = 50000;
        $scope.addMuffin.members = 10;
        $scope.addMuffin.frequency = 10;
        $scope.addMuffin.instalment = 5000;

        $scope.durationSelected = function(duration) {
            $scope.addMuffin.members = duration;
            $scope.addMuffin.frequency = duration;
            $scope.addMuffin.chitValue = $scope.addMuffin.instalment * $scope.addMuffin.frequency;
        };

        $scope.instalmentSelected = function(instalment) {
            $scope.addMuffin.instalment = instalment;
            $scope.addMuffin.chitValue = $scope.addMuffin.instalment * $scope.addMuffin.frequency;

        };

        // add new muffin Api Call
        $scope.addNewMuffin = function() {
            if ($scope.addMuffin.name && $scope.addMuffin.name.length > 0) {
                $scope.addMuffinProgress = true;
                // console.log($scope.addMuffin);

                network.addNewMuffinApiCall($scope.addMuffin)
                    .then(function(res) {
                        // console.log(res.data);
                        if (res.data && !res.data.isError) {

                            $('.muffinModal').appendTo("body").modal('hide');
                            swal({
                                title: 'Success!',
                                text: res.data.message,
                                type: 'success',
                                confirmButtonText: 'OK'
                            });
                            $scope.getMuffins();

                        } else if (res.data.isError) {
                            swal("Oops...", res.data.error, "error");
                        } else {
                            swal("Oops...", "Unable to parse server response", "error");
                        }
                        $scope.addMuffinProgress = false;
                    }, function(err) {
                        $scope.addMuffinProgress = false;
                        if (err.status === 401) {
                            swal("Oops...", err.data.error, "error");
                            $scope.logoutUser();

                        } else {
                            swal("Oops...", "Something went wrong. Please try again.", "error");
                        }
                    });
            } else {
                swal({
                    title: 'Error!',
                    text: "Please enter Muffin Name",
                    type: 'error',
                    confirmButtonText: 'OK'
                });
            }
        };

        // Download Muffin Agreement Api Call
        $scope.downloadAgreement = function() {
            $scope.agreementUrl = '';
            network.getAgreementApiCall($localStorage.userId)
                .then(function(res) {
                    // console.log(res.data);
                    if (res.data && !res.data.isError) {

                        if (res.data.data.muffinAgreements.length > 0) {
                            $scope.agreementUrl = 'https://s3-ap-southeast-1.amazonaws.com/muffinagreements/' + res.data.data.muffinAgreements[0].agreementLink;
                        }

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    } else {
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    // $scope.addMuffinProgress = false;
                }, function(err) {
                    // $scope.addMuffinProgress = false;
                    if (err.status === 401) {
                        swal("Oops...", err.data.error, "error");
                        $scope.logoutUser();

                    } else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        $scope.bidding = {};
        $scope.bidding.value = "";

        // Get current Auctions Api Call
        $scope.getCurrentAuctions = function(muffinId) {
            $scope.biddingMuffinBids = [];
            $scope.auctionId = "";
            $scope.highestBid = 0;
            $scope.showMembersProgress = true;
            network.getCurrentAuctionsApiCall(muffinId)
                .then(function(res) {
                    console.log('getCurrentAuctionsApiCall',res.data);
                    if (res.data && !res.data.isError) {

                        $scope.biddingMuffinSubmitBtn = false;

                        $scope.biddingMuffinBids = res.data.data.bids;
                        // console.log($scope.biddingMuffinBids);
                        for (var i = 0; i < $scope.biddingMuffinBids.length; i++) {
                            $scope.biddingMuffinBids[i].img = imgUrl + $scope.biddingMuffinBids[i].user_id._id;
                            // console.log($scope.biddingMuffinBids[i].img);

                        }

                        //TODO:
                        $("#bidding-time").countdown(
                            new Date().getTime() + res.data.data.timeLeft * 1000,
                            function(event) {
                                $(this).html(event.strftime('%H:%M:%S'));
                            }
                        );

                        // var deadline = new Date(Date.parse(new Date()) +  res.data.data.timeLeft);
                        // $scope.initializeClock(deadline, muffinId);

                        $scope.auctionId = res.data.data._id;
                        $scope.highestBid = res.data.data.highestBid.bid;

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    } else {
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showMembersProgress = false;
                }, function(err) {
                    $scope.showMembersProgress = false;
                    if (err.status === 401) {
                        swal("Oops...", err.data.error, "error");
                        $scope.logoutUser();

                    } else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        // Dismiss muffin in activities
        $scope.dismissMuffin = function(muffinIndex, muffinName) {
            $scope.activitiesMuffin.splice(muffinIndex, 1);

            ngToast.create({
                className: 'success',
                content: muffinName + ' has been deleted successfully',
                timeout: 1000,
                verticalPosition: 'bottom'
            });
        };

        //HELP
        $scope.faqSearch = "";

        $scope.selectHelp = function() {
            $scope.getFAQs();
            $scope.getVideos();
        };

        // Get FAQ Api Call
        $scope.getFAQs = function() {
            $scope.faqArray = [];
            $scope.showHelpProgress = true;
            network.getFAQsApiCall()
                .then(function(res) {
                    // console.log(res.data);
                    if (res.data && !res.data.isError) {

                        $scope.faqArray = res.data.data.results;

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    } else {
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showHelpProgress = false;
                }, function(err) {
                    $scope.showHelpProgress = false;
                    if (err.status === 401) {
                        swal("Oops...", err.data.error, "error");
                        $scope.logoutUser();

                    } else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        // Get Videos Api Call
        $scope.getVideos = function() {
            $scope.videosArray = [];
            $scope.showHelpProgress = true;
            network.getVideosApiCall()
                .then(function(res) {
                    // console.log(res.data);
                    if (res.data && !res.data.isError) {

                        $scope.videosArray = res.data.data.results;

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    } else {
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showHelpProgress = false;
                }, function(err) {
                    $scope.showHelpProgress = false;
                    if (err.status === 401) {
                        swal("Oops...", err.data.error, "error");
                        $scope.logoutUser();

                    } else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        $scope.selectedVideo = function(videoIndex) {
            var win = window.open($scope.videosArray[videoIndex].url, '_blank');
            if (win) {
                //Browser has allowed it to be opened
                win.focus();
            } else {
                //Browser has blocked it
                alert('Please allow popups for this website');
            }
        };

        $scope.animateToContactUs = function() {
            // console.log("scroll");
            $('html, body').animate({
                scrollTop: $('#contact').offset().top
            }, 'slow');
        };

        //contact Us Api Call
        $scope.contactUs = {};
        $scope.contactUs.topic = "";
        $scope.contactUs.message = "";
        $scope.contactUs.userId = $localStorage.userId;
        $scope.contactUs.userName = $localStorage.name;
        $scope.contactUs.userEmail = $localStorage.email;
        $scope.sendMessage = function() {
            if ($scope.contactUs.topic.length > 0) {
                if ($scope.contactUs.message.length > 0) {

                    // console.log($scope.contactUs);
                    $scope.showContactUsProgress = true;
                    network.contactUsApiCall($scope.contactUs)
                        .then(function(res) {
                            // console.log(res.data);
                            if (res.data && !res.data.isError) {

                                swal({
                                    title: 'Success!',
                                    text: res.data.data.serverMessage,
                                    type: 'success',
                                    confirmButtonText: 'OK'
                                });

                                $scope.contactUs.topic = "";
                                $scope.contactUs.message = "";

                            } else if (res.data.isError) {
                                swal("Oops...", res.data.error, "error");
                            } else {
                                swal("Oops...", "Unable to parse server response", "error");
                            }
                            $scope.showContactUsProgress = false;
                        }, function(err) {
                            $scope.showContactUsProgress = false;
                            if (err.status === 401) {
                                swal("Oops...", err.data.error, "error");
                                $scope.logoutUser();

                            } else {
                                swal("Oops...", "Something went wrong. Please try again.", "error");
                            }
                        });
                } else {
                    swal({
                        title: 'Error!',
                        text: "Please enter Message",
                        type: 'error',
                        confirmButtonText: 'OK'
                    });
                }
            } else {
                swal({
                    title: 'Error!',
                    text: "Please enter Title",
                    type: 'error',
                    confirmButtonText: 'OK'
                });
            }
        };

        // logout user
        $scope.logoutUser = function() {
            swal({
                title: 'Success!',
                text: "You have been successfully Logout",
                type: 'success',
                confirmButtonText: 'OK'
            });

            $localStorage.$reset();
            $localStorage.authToken = "";
            $localStorage.userId = "";
            $localStorage.name = "";
            $localStorage.email = "";

            $scope.$parent.goToLogin();
        };

        $(document).ready(function() { // this will be called when the DOM is ready
            $(".bid-inputs").keyup(function() {
                // console.log("value", this.value.length);
                // if (this.value.length == this.maxLength){
                // 	$(this).next('.bid-inputs').focus();
                // }
            });
        });

        $scope.googleplayUrl = function() {
            var win = window.open("https://play.google.com/store/apps/details?id=com.muffin.android", '_blank');
            if (win) {
                //Browser has allowed it to be opened
                win.focus();
            } else {
                //Browser has blocked it
                alert('Please allow popups for this website');
            }
        };

        $scope.connectSocket = function() {
            socket.on("connect", function() {
                // console.log("Socket Connected!");
            });

            socket.on("bid_update", function() {
                // console.log("bid update");
                $scope.getCurrentAuctions($scope.selectedMuffinId);
            });
        };

        $("#bidValue").keydown(function(e) {
            // console.log("num");
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl/cmd+A
                (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+C
                (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+X
                (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        // HOME
        $scope.selected = function(selected) {
            $scope.selectedTab = selected;

            if ($scope.selectedTab === 'muffins') {
                $scope.getMuffins();
                $scope.getActivitiesMuffins();
                $scope.getVideos();
                $scope.$parent.goToMuffins();

            } else if ($scope.selectedTab === 'reports') {
                $scope.$parent.goToReports();
            } else if ($scope.selectedTab === 'help') {
                $scope.$parent.goToHelp();
            }
        };

        $scope.selected('muffins');

        $scope.showHelp = true;
        $scope.dismissHelp = function() {
            $scope.showHelp = false;
        };

        $scope.showMuffinModal = function(type) {
            // console.log(type);
            $scope.muffinTabSelected = type;
            if (angular.equals(type, 'join')) {
                $scope.joinAvailableMuffin();
            }

            $('.muffinModal').appendTo("body").modal('show');

        };

        $scope.muffinModalCancel = function() {
            $('.muffinModal').appendTo("body").modal('hide');
        };

        // $('.muffinModal').on('hidden.bs.modal', function (e) {
        // 	$('body > .muffinModal').remove();
        // })

        $scope.muffinTabSelected = '';
        $scope.muffinFabClicked = function(type) {
            $scope.muffinTabSelected = type;
            if (angular.equals(type, 'add')) {

            } else if (angular.equals(type, 'join')) {
                $scope.joinAvailableMuffin();

            } else if (angular.equals(type, 'invite')) {

            }
        };

        $scope.dayNotification = false;
        $scope.hourNotification = false;
        $scope.toggleHourNotification = function() {
            $scope.hourNotification = !$scope.hourNotification;
            setTimeout(function() {
                swal({
                    title: 'Success!',
                    text: 'Successfully done',
                    type: 'success',
                    confirmButtonText: 'OK'
                });
            }, 1000);
        };

        $scope.toggleDayNotification = function() {
            $scope.dayNotification = !$scope.dayNotification;
            setTimeout(function() {
                swal({
                    title: 'Success!',
                    text: 'Successfully done',
                    type: 'success',
                    confirmButtonText: 'OK'
                });
            }, 1000);

        };

    }

})();