/**
 * Created by chitra on 14/10/17.
 */
(function() {
    'use strict';
    angular
        .module('app')
        .controller('ReportsController', ReportsController);

    ReportsController.$inject = ['$scope', '$state', '$timeout', '$http', 'network', '$localStorage'];

    function ReportsController($scope, $state, $timeout, $http, network, $localStorage) {
        $scope.showReceipt = true;
        $scope.balanceAmountPaid = 0;
        $scope.balanceAmountDue = 0;
        $scope.balanceAmountObj = {
                amountPaid: [0, 0, 0, 0, 0, 0],
                amountDue: [0, 0, 0, 0, 0, 0]
            }
            // Get active Muffins Api Call

        $scope.testClick = function() {
            //console.log('testsdf');
            // html2canvas(document.getElementById('exportthis'), {
            //     onrendered: function (canvas) {
            //         var data = canvas.toDataURL();
            //         var docDefinition = {
            //             content: [{
            //                 image: data,
            //                 width: 500,
            //             }]
            //         };
            //         pdfMake.createPdf(docDefinition).download("Score_Details.pdf");
            //     }
            // });
        }

        $scope.getMuffins = function() {
            $scope.reportMuffins = [];
            $scope.showProgress = true;

            network.getMuffins($scope.data)
                .then(function(res) {
                    // //console.log("all muffins",res.data);
                    if (res.data && !res.data.isError) {

                        $scope.muffins = res.data.data;
                        //console.log($scope.muffins);
                        for (var i = 0; i < $scope.muffins.length; i++) {
                            // //console.log($scope.muffins[i].type);
                            if ($scope.muffins[i].type === 'bidding_muffins' || $scope.muffins[i].type === 'active_muffins') {
                                if ($scope.muffins[i].muffin_details.length > 0) {
                                    $scope.reportMuffins.push($scope.muffins[i]);
                                }
                            }
                        }

                        if ($scope.reportMuffins.length > 0) {
                            $scope.muffinSelected(0, $scope.reportMuffins[0].muffin_details[0].muffin_detail._id);

                        }

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    } else {
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if (err.status === 401) {
                        swal("Oops...", err.data.error, "error");
                        $scope.logoutUser();

                    } else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        // muffin selected
        $scope.muffinSelected = function(index, muffinId) {
            $scope.selectedIndex = index;
            $scope.selectedMuffinId = muffinId;
            $scope.getActiveMuffinDetails();
            // //console.log( 'muffinId',muffinId);

            $scope.getAllPayments();
           // $scope.getTransactions();
        };

        // Get Muffin Details Api Call
        $scope.getActiveMuffinDetails = function() {
            $scope.auctions = [];
            $scope.showDetailsProgress = true;
            network.getInprocessMuffinsDetailsApiCall($scope.selectedMuffinId)
                .then(function(res) {
                     //console.log("MuffinDetails",res.data);
                    if (res.data && !res.data.isError) {
                        $scope.auctions = res.data.data.auctions;
                        sessionStorage.setItem('auctions', JSON.stringify($scope.auctions));

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    } else {
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showDetailsProgress = false;
                }, function(err) {
                    $scope.showDetailsProgress = false;
                    if (err.status === 401) {
                        swal("Oops...", err.data.error, "error");
                        $scope.logoutUser();

                    } else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        $scope.getMuffins();


        $scope.reportTypeSelected = function(type) {
            var app = angular.module('myApp', []);
            app.controller('datCtrl', function($scope) {
            $scope.today = new Date();
          

            });

            if (angular.equals(type, 'auction')) {
                  $scope.today = new Date();
                $scope.showAuction = $scope.showAuction != true;
                $scope.showTransaction = false;
                $scope.showStatement = false;

            } else if (angular.equals(type, 'statement')) {
            
                $scope.showStatement = $scope.showStatement != true;
                $scope.showTransaction = false;
                $scope.showAuction = false;
                $scope.getAllPayments();

            } else if (angular.equals(type, 'transaction')) {
               
                $scope.showTransaction = $scope.showTransaction != true;
                $scope.showAuction = false;
                $scope.showStatement = false;
                $scope.getTransactions();
            }
        };

        $scope.reportTypeSelected('auction');

        //get late fee amount
        $scope.getLateFee = function() {
            var data = {};
            data.userId = $localStorage.userId;
            data.muffinId = $scope.selectedMuffinId;
            $scope.today = new Date();
            $scope.const ='10';
            $scope.month= $scope.today.getMonth()+1;
            $scope.year = $scope.today.getFullYear();
           
            $scope.day=$scope.today.getDate();
            $scope.mon=['Jan','Fab','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Des'];
            if($scope.day<=10){
                var dateObj = new Date();
                    if($scope.month ==12 && $scope.day>10){
                     $scope.month=1;
                     $scope.year=$scope.year+1;
                    }

                var day = dateObj.getUTCDate();
                var year = dateObj.getUTCFullYear();

                $scope.duedate =  $scope.mon[$scope.month-1] + " " + $scope.const + "," + year;
            } else if($scope.day>10){
                  var dateObj = new Date();

                   if(dateObj.getUTCMonth() ==12){
                    $scope.month=1;
                    }else{
                    $scope.month=$scope.month+1;
                    }

              
                var day = dateObj.getUTCDate();
                var year = dateObj.getUTCFullYear();

                $scope.duedate =  $scope.mon[$scope.month-1] + " " + $scope.const + "," + $scope.year;

            }




            console.log("===== date===>"+$scope.today);
            console.log("Today month===>"+$scope.month);
            console.log("due Date day===>"+$scope.duedate);
            $scope.globalArray = [];
            $scope.lateFee = [];
            network.getLateFeeApiCall(data)
                .then(function(res) {
                    //console.log("late fee",res.data);
                    if (res.data && res.data.status == 'success') {

                        // //console.log("usrId", $localStorage.userId);

                        if (res.data.response.length > 0) {
                            $scope.lateFee = res.data.response;
                            $scope.lateFee.forEach(function(obj) { obj.description = "Late Fee"; });
                            $scope.globalArray = $scope.globalArray.concat($scope.lateFee);
                           

                            if ($scope.lateFee[0].amountPaid) {
                                $scope.balanceAmountObj.amountPaid[0] = $scope.lateFee[0].amountPaid;
                                //$scope.balanceAmountPaid = $scope.balanceAmountPaid + $scope.lateFee[0].amountPaid;
                            } else {
                                $scope.balanceAmountObj.amountPaid[0] = 0;
                            }
                            if ($scope.lateFee[0].amountDue) {
                                $scope.balanceAmountObj.amountDue[0] = $scope.lateFee[0].amountDue;
                               // $scope.balanceAmountDue = $scope.balanceAmountDue + $scope.lateFee[0].amountdue;
                            } else {
                                $scope.balanceAmountObj.amountDue[0] = 0;
                            }

                            //console.log($scope.lateFee, 'lateFee');
                        } else {
                            $scope.balanceAmountObj.amountPaid[0] = 0;
                            $scope.balanceAmountObj.amountDue[0] = 0;
                        }

                    } else if (res.data.status == 'error') {
                        $scope.balanceAmountObj.amountPaid[0] = 0;
                        $scope.balanceAmountObj.amountDue[0] = 0;
                        swal("Oops...", res.data.status, "error");
                    } else {
                        $scope.balanceAmountObj.amountPaid[0] = 0;
                        $scope.balanceAmountObj.amountDue[0] = 0;
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if (err.status === 401) {
                        swal("Oops...", err.data.status, "error");
                        $scope.logoutUser();

                    } else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });

            $scope.getBounceFee();

        };

        //get bounce fee amount
        $scope.getBounceFee = function() {
            var data = {};
            data.userId = $localStorage.userId;
            data.muffinId = $scope.selectedMuffinId;
            //console.log("before called bounce api");
            $scope.bounceFee = [];
            network.getBounceFeeApiCall(data)
                .then(function(res) {
                     //console.log("bounce fee",res.data);
                    if (res.data && res.data.status == 'success') {

                        if (res.data.response.length > 0) {
                            $scope.bounceFee = res.data.response;
                            $scope.bounceFee.forEach(function(obj) { obj.description = "Bounce Charges"; });
                            $scope.globalArray = $scope.globalArray.concat($scope.bounceFee);
                           
                            if ($scope.bounceFee[0].amountPaid) {
                                $scope.balanceAmountObj.amountPaid[1] = $scope.bounceFee[0].amountPaid;
                               // $scope.balanceAmountPaid = $scope.balanceAmountPaid + $scope.bounceFee[0].amountPaid;
                            } else {
                                $scope.balanceAmountObj.amountPaid[1] = 0;
                            }
                            if ($scope.bounceFee[0].amountDue) {
                                $scope.balanceAmountObj.amountDue[1] = $scope.bounceFee[0].amountDue;
                               // $scope.balanceAmountDue = $scope.balanceAmountDue + $scope.bounceFee[0].amountDue;
                            } else {
                                $scope.balanceAmountObj.amountDue[1] = 0;
                            }
                            //console.log($scope.bounceFee, 'bounceFee');
                        } else {
                            $scope.balanceAmountObj.amountPaid[1] = 0;
                            $scope.balanceAmountObj.amountDue[1] = 0;
                        }
                    } else if (res.data.status == 'error') {
                        $scope.balanceAmountObj.amountPaid[1] = 0;
                        $scope.balanceAmountObj.amountDue[1] = 0;
                        swal("Oops...", res.data.status, "error");
                    } else {
                        $scope.balanceAmountObj.amountPaid[1] = 0;
                        $scope.balanceAmountObj.amountDue[1] = 0;
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if (err.status === 401) {
                        swal("Oops...", err.data.status, "error");
                        $scope.logoutUser();

                    } else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
            $scope.getDelayedPaymentFee();

        };

        //get Delayed Payment fee amount
        $scope.getDelayedPaymentFee = function() {
            var data = {};
            data.userId = $localStorage.userId;
            data.muffinId = $scope.selectedMuffinId;
            //console.log("before called delayed payment api");
            $scope.delayedPaymentFee = [];
            network.getDelayedPaymentFeeApiCall(data)  
                .then(function(res) {
                     //console.log("delayed payment fee",res.data);
                    if (res.data && res.data.status == 'success') {

                        if (res.data.response.length > 0) {

                            $scope.delayedPaymentFee = res.data.response;
                            $scope.delayedPaymentFee.forEach(function(obj) { obj.description = "Interest on Late Payment"; });
                            $scope.globalArray = $scope.globalArray.concat($scope.delayedPaymentFee);
                            if ($scope.delayedPaymentFee[0].amountPaid) {
                                $scope.balanceAmountObj.amountPaid[2] = $scope.delayedPaymentFee[0].amountPaid;
                               // $scope.balanceAmountPaid = $scope.balanceAmountPaid + $scope.delayedPaymentFee[0].amountPaid;
                            } else {
                                $scope.balanceAmountObj.amountPaid[2] = 0;
                            }
                            if ($scope.delayedPaymentFee[0].amountDue) {
                                $scope.balanceAmountObj.amountDue[2] = $scope.delayedPaymentFee[0].amountDue;
                                //$scope.balanceAmountDue = $scope.balanceAmountDue + $scope.delayedPaymentFee[0].amountdue;
                            } else {
                                $scope.balanceAmountObj.amountDue[2] = 0;
                            }
                            //console.log($scope.delayedPaymentFee, 'delayedPaymentFee')
                        } else {
                            $scope.balanceAmountObj.amountPaid[2] = 0;
                            $scope.balanceAmountObj.amountDue[2] = 0;
                        }

                    } else if (res.data.status == 'error') {
                        $scope.balanceAmountObj.amountPaid[2] = 0;
                        $scope.balanceAmountObj.amountDue[2] = 0;
                        swal("Oops...", res.data.status, "error");
                    } else {
                        $scope.balanceAmountObj.amountPaid[2] = 0;
                        $scope.balanceAmountObj.amountDue[2] = 0;
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if (err.status === 401) {
                        swal("Oops...", err.data.status, "error");
                        $scope.logoutUser();

                    } else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
            $scope.getInstalmentPayableFee();

        };

        //get Instalment Payable fee amount
        $scope.getInstalmentPayableFee = function() {
            var data = {};
            data.userId = $localStorage.userId;
            data.muffinId = $scope.selectedMuffinId;
            //console.log("before called installment paybable api");
            //console.log(data);
            $scope.instalmentPayableFee = [];
            network.getInstalmentPayableFeeApiCall(data)
                .then(function(res) {
                     //console.log("instalment payable fee",res.data);
                    if (res.data && res.data.status == 'success') {

                        if (res.data.response.length > 0) {

                            $scope.instalmentPayableFee = res.data.response;
                            $scope.instalmentPayableFee.forEach(function(obj) { obj.description = "Instalment Payable"; });
                            $scope.globalArray = $scope.globalArray.concat($scope.instalmentPayableFee);
                            if ($scope.instalmentPayableFee[0].amountPaid) {
                                $scope.balanceAmountObj.amountPaid[3] = $scope.instalmentPayableFee[0].amountPaid;
                              //  $scope.balanceAmountPaid = $scope.balanceAmountPaid + $scope.instalmentPayableFee[0].amountPaid;
                            } else {
                                $scope.balanceAmountObj.amountPaid[3] = 0;
                            }
                            if ($scope.instalmentPayableFee[0].amountDue) {
                                $scope.balanceAmountObj.amountDue[3] = $scope.instalmentPayableFee[0].amountDue;
                              //  $scope.balanceAmountDue = $scope.balanceAmountDue + $scope.instalmentPayableFee[0].amountdue;
                            } else {
                                $scope.balanceAmountObj.amountDue[3] = 0;
                            }

                            //console.log($scope.instalmentPayableFee, 'istalmentPayableFee');
                        } else {
                            $scope.balanceAmountObj.amountPaid[3] = 0;
                            $scope.balanceAmountObj.amountDue[3] = 0;
                        }

                    } else if (res.data.status == 'error') {
                        $scope.balanceAmountObj.amountPaid[3] = 0;
                        $scope.balanceAmountObj.amountDue[3] = 0;
                        swal("Oops...", res.data.status, "error");
                    } else {
                        $scope.balanceAmountObj.amountPaid[3] = 0;
                        $scope.balanceAmountObj.amountDue[3] = 0;
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if (err.status === 401) {
                        swal("Oops...", err.data.status, "error");
                        $scope.logoutUser();

                    } else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
            $scope.getAmountPaidFee();

        };

        //get Amount Paid fee amount
        $scope.getAmountPaidFee = function() {
            var data = {};
            data.userId = $localStorage.userId;
            data.muffinId = $scope.selectedMuffinId;
            //console.log("before called amountPaid api");
            //console.log(data);
            $scope.amountPaidFee = [];
            network.getAmountPaidFeeApiCall(data)
                .then(function(res) {
                     //console.log("amount paid fee",res.data);
                    if (res.data && res.data.status == 'success') {

                        if (res.data.response.length > 0) {
                            $scope.amountPaidFee = res.data.response;
                            $scope.amountPaidFee.forEach(function(obj) { obj.description = "Amount Paid"; });
                            $scope.globalArray = $scope.globalArray.concat($scope.amountPaidFee);
                            console.log("------->"+$scope.globalArray);
                            if ($scope.amountPaidFee[0].amountPaid) {
                                $scope.balanceAmountObj.amountPaid[4] = $scope.amountPaidFee[0].amountPaid;
                              //  $scope.balanceAmountPaid = $scope.balanceAmountPaid + $scope.amountPaidFee[0].amountPaid;
                            } else {
                                $scope.balanceAmountObj.amountPaid[4] = 0;
                            }
                            if ($scope.amountPaidFee[0].amountDue) {
                                $scope.balanceAmountObj.amountDue[4] = $scope.amountPaidFee[0].amountDue;
                              //  $scope.balanceAmountDue = $scope.balanceAmountDue + $scope.amountPaidFee[0].amountdue;
                            } else {
                                $scope.balanceAmountObj.amountDue[4] = 0;
                            }

                            //console.log($scope.amountPaidFee);
                        } else {
                            $scope.balanceAmountObj.amountPaid[4] = 0;
                            $scope.balanceAmountObj.amountDue[4] = 0;
                        }

                    } else if (res.data.status == 'error') {
                        $scope.balanceAmountObj.amountPaid[4] = 0;
                        $scope.balanceAmountObj.amountDue[4] = 0;
                        swal("Oops...", res.data.status, "error");
                    } else {
                        $scope.balanceAmountObj.amountPaid[4] = 0;
                        $scope.balanceAmountObj.amountDue[4] = 0;
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if (err.status === 401) {
                        swal("Oops...", err.data.status, "error");
                        $scope.logoutUser();

                    } else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
           // $scope.getBalanceAmount();
           $scope.getDividendReceivedFee();
        };


        // get Dividend Received Details

        $scope.getDividendReceivedFee = function() {
            var data = {};
            data.userId = $localStorage.userId;
            data.muffinId = $scope.selectedMuffinId;
            //console.log("before called dividend received api");
            //console.log(data);
            $scope.dividendReceived = [];
            network.getDividendReceivedFeeApiCall(data)
                .then(function(res) {
                    //console.log("dividend received find by id called",res.data);
                    if (res.data && res.data.status == 'success') {
                        if (res.data.response.length > 0) {
                            $scope.dividendReceived = res.data.response;
                            $scope.dividendReceived.forEach(function(obj) { obj.description = "Dividend Received"; });
                            $scope.globalArray = $scope.globalArray.concat($scope.dividendReceived);
                            console.log("------->"+$scope.globalArray);
                            if ($scope.dividendReceived[0].amountPaid) {
                                $scope.balanceAmountObj.amountPaid[5] = $scope.dividendReceived[0].amountPaid;
                               // $scope.balanceAmountPaid = $scope.balanceAmountPaid + $scope.dividendReceived[0].amountPaid;
                            }
                            if ($scope.dividendReceived[0].amountDue) {
                                $scope.balanceAmountObj.amountDue[5] = $scope.dividendReceived[0].amountDue;
                                 // $scope.balanceAmountDue = $scope.balanceAmountDue + $scope.dividendReceived[0].amountDue;
                            }

                            //console.log($scope.dividendReceived);
                        }

                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.status, "error");
                    } else {
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if (err.status === 401) {
                        swal("Oops...", err.data.status, "error");
                        $scope.logoutUser();

                    } else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
                $scope.getBalanceAmount();
        };

       // get Balance amount
        $scope.getBalanceAmount = function() {
            var data = {};
            //console.log('balance data');
            //my code
            $scope.balanceAmountPaid = 0;
            $scope.balanceAmountDue = 0;
            angular.forEach($scope.balanceAmountObj.amountPaid, function(value) {
                $scope.balanceAmountPaid = $scope.balanceAmountPaid + value;
            });
            angular.forEach($scope.balanceAmountObj.amountDue, function(value) {
                $scope.balanceAmountDue = $scope.balanceAmountDue + value;
            });

            data.userId = $localStorage.userId;
            data.muffinId = $scope.selectedMuffinId;

            $scope.balanceAmount = [];
            network.getDividendReceivedFeeApiCallforBal(data)
                .then(function(res) {
                    //console.log("balance amount",res.data.res);
                    if (res.data && res.data.status == 'success') {
                        //console.log("Response data"+res);
                        $scope.balanceAmount = res.data.res;
                        // console.log($scope.globalArray);
                         // if ($scope.balanceAmount[0].amountPaid) {
                         //    $scope.balanceAmountPaid = $scope.balanceAmountPaid + $scope.balanceAmount[0].amountPaid;
                         // }
                         // if ($scope.balanceAmount[0].amountdue) {
                         //    $scope.balanceAmountDue = $scope.balanceAmountDue + $scope.balanceAmount[0].amountdue;
                         // }
                        //console.log("total balance--"+$scope.balanceAmount.balanceAmt);
                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.status, "error");
                    } else {
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showProgress = false;
                }, function(err) {
                    $scope.showProgress = false;
                    if (err.status === 401) {
                        swal("Oops...", err.data.status, "error");
                        $scope.logoutUser();

                    } else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        $scope.txnDetailsGlobal;
        $scope.updateModalValue = function(txnDetails) {
            $scope.txnDetailsGlobal = txnDetails;
        }

        $scope.getAllPayments = function() {

           $scope.getLateFee();
          //  $scope.getBounceFee();
          //  $scope.getDelayedPaymentFee();
          //   $scope.getInstalmentPayableFee();
          //  $scope.getAmountPaidFee();
          //  $scope.getBalanceAmount();
          //   $scope.getDividendReceivedFee();
          // $scope.getBalanceAmount();

        };

        //getTransactions
        $scope.getTransactions = function() {
            $scope.transactions = [];
            $scope.showTransactionProgress = false;
            network.getTransactionsApiCall()
                .then(function(res) {
                    // //console.log("trans id",res.data);
                    if (res.data && res.data.status == 'success') {

                        if (res.data.response.length > 0) {
                            for (var i = 0; i < res.data.response.length; i++) {
                                if (angular.equals(res.data.response[i].userId, $localStorage.userId) &&
                                    angular.equals(res.data.response[i].muffinId, $scope.selectedMuffinId)) {

                                    $scope.transactions.push(res.data.response[i]);
                                    //console.log($scope.transactions, 'transactions');
                                    //console.log($scope.transactions);
                                }
                            }
                        }

                    } else if (res.data.status == 'error') {
                        swal("Oops...", res.data.status, "error");
                    } else {
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showTransactionProgress = false;
                }, function(err) {
                    $scope.showTransactionProgress = false;
                    if (err.status === 401) {
                        swal("Oops...", err.data.status, "error");
                        $scope.logoutUser();

                    } else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

    }

})();
