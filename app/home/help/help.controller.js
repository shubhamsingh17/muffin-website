/**
 * Created by chitra on 14/10/17.
 */
(function() {
    'use strict';
    angular
        .module('app')
        .controller('HelpController', HelpController);

    HelpController.$inject = ['$scope', '$state', '$timeout','$http','network','$localStorage'];

    function HelpController($scope, $state, $timeout, $http, network, $localStorage) {

        // console.log('help help help!!!');
        $scope.faqSearch = "";

        // Get FAQ Api Call
        $scope.getFAQs = function () {
            $scope.faqArray = [];
            $scope.showHelpProgress = true;
            network.getFAQsApiCall()
                .then(function(res) {
                    // console.log(res.data);
                    if (res.data && !res.data.isError) {

                        $scope.faqArray = res.data.data.results;

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }
                    $scope.showHelpProgress = false;
                }, function(err) {
                    $scope.showHelpProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        //contact Us Api Call
        $scope.contactUs = {};
        $scope.contactUs.topic = "";
        $scope.contactUs.message = "";
        $scope.contactUs.userId = $localStorage.userId;
        $scope.contactUs.userName = $localStorage.name;
        $scope.contactUs.userEmail = $localStorage.email;
        $scope.sendMessage = function () {
            if($scope.contactUs.topic.length > 0){
                if($scope.contactUs.message.length > 0){

                    // console.log($scope.contactUs);
                    $scope.showContactUsProgress = true;
                    network.contactUsApiCall($scope.contactUs )
                        .then(function(res) {
                            // console.log(res.data);
                            if (res.data && !res.data.isError) {

                                swal({
                                    title: 'Success!',
                                    text: res.data.data.serverMessage,
                                    type: 'success',
                                    confirmButtonText: 'OK'
                                });

                                $scope.contactUs.topic = "";
                                $scope.contactUs.message = "";

                            } else if (res.data.isError) {
                                swal("Oops...", res.data.error, "error");
                            }else{
                                swal("Oops...", "Unable to parse server response", "error");
                            }
                            $scope.showContactUsProgress = false;
                        }, function(err) {
                            $scope.showContactUsProgress = false;
                            if(err.status === 401){
                                swal("Oops...", err.data.error, "error");
                            }else {
                                swal("Oops...", "Something went wrong. Please try again.", "error");
                            }
                        });
                }else {
                    swal({
                        title: 'Error!',
                        text: "Please enter Message",
                        type: 'error',
                        confirmButtonText: 'OK'
                    });
                }
            }else {
                swal({
                    title: 'Error!',
                    text: "Please enter Title",
                    type: 'error',
                    confirmButtonText: 'OK'
                });
            }
        };

        // Get Videos Api Call
        $scope.getVideos = function () {
            $scope.videosArray = [];
            $scope.showHelpProgress = true;
            network.getVideosApiCall()
                .then(function(res) {
                    // console.log(res.data);
                    if (res.data && !res.data.isError) {

                        $scope.videosArray = res.data.data.results;

                    } else if (res.data.isError) {
                        swal("Oops...", res.data.error, "error");
                    }else{
                        swal("Oops...", "Unable to parse server response", "error");
                    }

                    $scope.showHelpProgress = false;
                }, function(err) {
                    $scope.showHelpProgress = false;
                    if(err.status === 401){
                        swal("Oops...", err.data.error, "error");
                    }else {
                        swal("Oops...", "Something went wrong. Please try again.", "error");
                    }
                });
        };

        $scope.selectedVideo = function (videoIndex) {
            var win = window.open($scope.videosArray[videoIndex].url, '_blank');
            if (win) {
                //Browser has allowed it to be opened
                win.focus();
            } else {
                //Browser has blocked it
                alert('Please allow popups for this website');
            }
        };

        $scope.animateToContactUs = function(){
            // console.log("scroll");
            $('.muffins-body').animate({
                scrollTop: $('#contact').offset().top
            }, 'slow');
        };

        $scope.getFAQs();
        $scope.getVideos();

        $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
            $('.single-item').css("visibility","hidden");
            $timeout(function() {
                $(".single-item").slick({
                    dots: true,
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    autoplay: true,
                    autoplaySpeed: 3000
                });
                $('.single-item').css("visibility","visible");
            }, 1000);
        });
    }

})();