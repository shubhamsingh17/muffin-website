(function() {
	'use strict';
	angular
		.module('app')
		.factory('network', network);

	network.$inject = ['$http','$localStorage'];

	function network($http,$localStorage) {

		var baseUrl = "http://ec2-13-229-163-194.ap-southeast-1.compute.amazonaws.com:9000/";
		  // var baseUrl = "http://localhost:9000/";
	    // var icaurl= "http://localhost/ica-dashboard/api/experian/getCreditScore.php";
		var icaurl= "http://18.218.64.26:9000/api/experian/getCreditScore.php";
		var imgUrl = "https://s3-ap-southeast-1.amazonaws.com/muffdata/";
		var network = {};

		//signup
		network.signupApiCall = function(data) {
			return $http({
				method: 'POST',
				url: baseUrl + 'api/users/create',
				headers: {
					'Content-Type': 'application/json'
				},
				data: data
			});
		};

		//login
		network.loginApiCall = function(data) {
			return $http({
				method: 'POST',
				url: baseUrl + 'api/users/loginUserName',
				headers: {
					'Content-Type': 'application/json'
				},
				data: data
			});
		};

		//forgot Password
		network.userExistApiCall = function(data) {
			return $http({
				method: 'POST',
				url: baseUrl + 'api/users/userExists',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + $localStorage.authToken
				},
				data: data
			});
		};

		//TODO: check api
		//set password and pin
		network.setPasswordAndPinApiCall = function(data) {
			return $http({
				method: 'POST',
				url: baseUrl + 'api/users/setPasswordPin',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + $localStorage.authToken
				},
				data: data
			});
		};

		//send otp
		network.sendOtpApiCall = function(otpValue, mobile) {
			return $http({
				method: 'GET',
				url: 'http://103.16.101.52:8080/sendsms/bulksms?username=thcl-muffin&password=Muffin12&type=0&dlr=1&destination=91'+mobile+'&source=MUFFIN&message=OTP%20for%20Muffin%20is%20'+otpValue+'.%20For%20your%20security,%20please%20do%20not%20share%20this%20message%20with%20anyone',
				headers: {
					'Content-Type': 'application/json'
				}
			});
		};

		//verify otp
		network.verifyOtpApiCall = function(data) {
			return $http({
				method: 'POST',
				url: baseUrl + 'api/users/verifyOtp',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + $localStorage.authToken
				},
				data: data
			});
		};

		//set pin
		network.setPinApiCall = function(data) {
			return $http({
				method: 'POST',
				url: baseUrl + 'api/users/setPin',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + $localStorage.authToken
				},
				data: data
			});
		};

		//Get muffins
		network.getMuffins = function() {
			return $http({
				method: 'GET',
				url: baseUrl + 'api/muffins-for-users/get/users/muffins',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + $localStorage.authToken
				}
			});
		};

		//Add muffins
		network.addNewMuffinApiCall = function(data) {
			return $http({
				method: 'POST',
				url: baseUrl + 'api/muffins/create',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + $localStorage.authToken
				},
				data: data
			});
		};

		// Invited Muffins- accept, decline
		network.acceptDeclineApiCall = function(data) {
			return $http({
				method: 'PUT',
				url: baseUrl + 'api/muffins-for-users/accept/invite',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + $localStorage.authToken
				},
				data: data
			});
		};

		// Get muffin Inprocess details
		network.getInprocessMuffinsDetailsApiCall = function(muffinId) {
			return $http({
				method: 'GET',
				url: baseUrl + 'api/muffins-for-users/getMuffinDetailApp/' + muffinId,
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + $localStorage.authToken
				}
			});
		};

		// Get muffin members
		network.getMuffinsMembersApiCall = function(muffinId) {
			return $http({
				method: 'GET',
				url: baseUrl + 'api/muffins-for-users/getMuffinMembersMobile/' + muffinId,
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + $localStorage.authToken
				}
			});
		};

		// Get available muffin
		network.getAvailableMuffinsApiCall = function(userId) {
			return $http({
				method: 'GET',
				url: baseUrl + 'api/muffins-for-users/getAvailableMuffins/' + userId,
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + $localStorage.authToken
				}
			});
		};

		// add Invite muffin
		network.addInvitedMuffinApiCall = function(data) {
			return $http({
				method: 'PUT',
				url: baseUrl + 'api/muffins-for-users/add/invite',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + $localStorage.authToken
				},
				data: data
			});
		};

		// schedule meeting
		network.scheduleMeetingApiCall = function(data) {
			return $http({
				method: 'PUT',
				url: baseUrl + 'api/muffins-for-users/reschedule/muffin/meeting',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + $localStorage.authToken
				},
				data: data
			});
		};

		// Get agreement
		network.getAgreementApiCall = function(userId) {
			return $http({
				method: 'GET',
				url: baseUrl + 'api/muffins-for-users/getAgreements/' + userId,
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + $localStorage.authToken
				}
			});
		};

		// Get Current auction
		network.getCurrentAuctionsApiCall = function(muffinId) {
			return $http({
				method: 'GET',
				url: baseUrl + 'api/auctions/getCurrentAuction/' + muffinId,
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + $localStorage.authToken
				}
			});
		};

		//Get Profile
		network.getProfileApiCall = function(userId) {
			return $http({
				method: 'GET',
				url: baseUrl + 'api/users/get/my/profile/' + userId,
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + $localStorage.authToken
				}
			});
		};

		// Edit Profile Request
		network.editProfileRequestApiCall = function(userId) {
			return $http({
				method: 'PUT',
				url: baseUrl + 'api/users/edit/my/profile/bank/' + userId,
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + $localStorage.authToken
				}
			});
		};

		// FAQ
		network.getFAQsApiCall = function() {
			return $http({
				method: 'GET',
				url: baseUrl + 'api/muffins-for-users/get/muffin/faq',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + $localStorage.authToken
				}
			});
		};

		// get Videos
		network.getVideosApiCall = function() {
			return $http({
				method: 'GET',
				url: baseUrl + 'api/users/getVideos',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + $localStorage.authToken
				}
			});
		};

		// Contact Us
		network.contactUsApiCall = function(data) {
			return $http({
				method: 'POST',
				url: baseUrl + 'api/contact-us/create',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + $localStorage.authToken
				},
				data: data
			});
		};

		// Reports
		// get late fee amount
		network.getLateFeeApiCall = function(data) {
			return $http({
				method: 'POST',
				url: baseUrl + 'api/reports-latefeecharges/findbyid',
				headers: {
					'Content-Type': 'application/json'
				},
				data: data
			});
		};

		// get bounce fee amount
		network.getBounceFeeApiCall = function(data) {
			return $http({
				method: 'POST',
				url: baseUrl + 'api/reports-bouncecharges/findbyid',
				headers: {
					'Content-Type': 'application/json'
				},
				data: data
			});
		};

		// get delayed payment interest amount
		network.getDelayedPaymentFeeApiCall = function(data) {
			return $http({
				method: 'POST',
				url: baseUrl + 'api/reports-delayedpaymentinterest/findbyid',
				headers: {
					'Content-Type': 'application/json'
				},
				data: data
			});
		};

		// get instalment payable fee amount
		network.getInstalmentPayableFeeApiCall = function(data) {
			return $http({
				method: 'POST',
				url: baseUrl + 'api/reports-installmentpayable/findbyid',
				headers: {
					'Content-Type': 'application/json'
				},
				data: data
			});
		};// get amount paid amount
		network.getAmountPaidFeeApiCall = function(data) {
			return $http({
				method: 'POST',
				url: baseUrl + 'api/reports-amountpaid/findbyid',
				headers: {
					'Content-Type': 'application/json'
				},
				data: data
			});
		};
		//get balance amount
		network.getDividendReceivedFeeApiCallforBal = function(data) {
			return $http({
				method: 'POST',
				url: baseUrl + 'api/reports-dividendreceived/findData',
				headers: {
					'Content-Type': 'application/json'
				},
				data: data
			});
		};
		 network.getDividendReceivedFeeApiCall = function(data) {
			 return $http({
				method: 'POST',
				url: baseUrl + 'api/reports-dividendreceived/findbyid',
				//url: baseUrl + 'api/reports-dividendreceived/findData',
				headers: {
					'Content-Type': 'application/json'
				},
				data: data
			});
		 };

		// get reports
		network.getTransactionsApiCall = function() {
			return $http({
				method: 'GET',
				url: baseUrl + 'api/report',
				headers: {
					'Content-Type': 'application/json'
				}
			});
		};

		// get credit report
		network.getCreditReport = function(obj) {
			return $http({
				method: 'POST',
				url: icaurl,
				headers: {
					'Content-Type': 'application/json'
				},
				data:obj
			});
		};

		return network;
	}
})();
