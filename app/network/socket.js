(function() {
	'use strict';
	angular
	.module('app')
	.factory('socket', socket);


	function socket(socketFactory) {
		return socketFactory({
			sioSocket: io.connect('http://ec2-13-229-163-194.ap-southeast-1.compute.amazonaws.com:9000/')
		});
	}
})();

13-229-163-194