var myApp = angular
.module('app', [
	'ui.router',
	'slick',
	'rzModule',
	'angularSlideables',
    'ngMaterial',
    'ngAnimate',
    'ngAria',
    'ngMessages',
    'ngStorage',
    'facebook',
    'ui.bootstrap',
	'ngMaterialDatePicker',
	'mdPickers',
	'btford.socket-io',
	'ngSanitize',
	'ngToast'
])
    
    .config(function(FacebookProvider) {
        FacebookProvider.init('467653353577660');
    });
    
    myApp.config(function($stateProvider, $urlRouterProvider) {

			$stateProvider
				.state('login', {
					url: '/login',
					templateUrl: 'app/login/login.html',
					controller: 'LoginController'
				})
				.state('signup', {
					url: '/signup/:name/:email/:profileImg',
					templateUrl: 'app/signup/signup.html',
					controller: 'SignupController'
				})
				.state('home', {
					abstract: true,
					url: '/home',
					templateUrl: 'app/home/home.html',
					controller: 'HomeController'
				})
				.state('home.muffins', {
					url: '/muffins',
					templateUrl: 'app/home/muffin/muffin.html',
					controller: 'MuffinController'
				})
				.state('home.reports', {
					url: '/reports',
					templateUrl: 'app/home/reports/reports.html',
					controller: 'ReportsController'
				})
				.state('home.help', {
					url: '/help',
					templateUrl: 'app/home/help/help.html',
					controller: 'HelpController'
				})
				.state('profile', {
					url: '/profile',
					templateUrl: 'app/profile/profile.html',
					controller: 'ProfileController'
				});

			$urlRouterProvider.otherwise('/login');
		});

	myApp.filter('capitalize', function() {
		return function(input, all) {
			var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
			return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
		}
	});

	myApp.directive('errSrc', function() {
		return {
			link: function(scope, element, attrs) {
				element.bind('error', function() {
					if (attrs.src != attrs.errSrc) {
						attrs.$set('src', attrs.errSrc);
					}
				});
			}
		}
	});
