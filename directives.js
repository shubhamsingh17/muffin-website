(function() {
    'use strict';
    angular
        .module('app')
        .directive('onFinishRender', function($timeout) {
            return {
                restrict: 'A',
                link: function(scope, element, attr) {
                    if (scope.$last === true) {
                        $timeout(function() {
                            scope.$emit(attr.onFinishRender);
                        });
                    }
                }
            }
        })
        .directive('onFinishRenderTwo', function($timeout) {
            return {
                restrict: 'A',
                link: function(scope, element, attr) {
                    if (scope.$last === true) {
                        $timeout(function() {
                            scope.$emit(attr.onFinishRenderTwo);
                        });
                    }
                }
            }
        })
        .directive('onFinishRenderThree', function($timeout) {
            return {
                restrict: 'A',
                link: function(scope, element, attr) {
                    if (scope.$last === true) {
                        $timeout(function() {
                            scope.$emit(attr.onFinishRenderThree);
                        });
                    }
                }
            }
        });
})();